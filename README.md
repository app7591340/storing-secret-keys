## 参考

* https://guides.codepath.com/android/storing-secret-keys-in-android
* https://stackoverflow.com/questions/77224069/saving-api-keys-on-local-properties-file-to-avoid-version-control-checking-in
* https://research.miidas.jp/2020/03/android-%E3%83%AA%E3%83%90%E3%83%BC%E3%82%B9%E3%82%A8%E3%83%B3%E3%82%B8%E3%83%8B%E3%82%A2%E3%83%AA%E3%83%B3%E3%82%B0%E3%80%80%E5%85%A5%E9%96%80/
* https://qiita.com/goofmint/items/beb81edbefd252de9ba2
* https://toconakis.tech/androidautobackup/
* https://developers-jp.googleblog.com/2019/04/new-keystore-features-keep-your-slice.html

