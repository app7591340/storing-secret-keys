package com.example.storingsecretkeys;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String buildConfigKey = BuildConfig.PROPERTIES_API_SECRET;
        final String resourceKey = getString(R.string.resource_api_secret);
    }
}